# Line Of Business - Wiki

## Índice

1. Alcance
2. [Parte técnia](wiki/parte-tecnica.md)
  1. [Arquitectura](wiki/parte-tecnica/arquitectura.md)
  2. Guía de desarrollo
      1. [Estructura de una solución Visual Studio para una aplicación](wiki/parte-tecnica/guia-de-desarrollo/estructura-de-una-solucion.md)
      2. [Proyectos](wiki/parte-tecnica/guia-de-desarrollo/proyectos.md)
      3. Nomenclatura de clases.
3. Parte funcional
  1. Áreas de negocio
  2. Procesos


## ¿Como colaborar?

El proyecto LoB se alberga en los servidores de GitLab. Dicho proyecto se organiza de la misma forma que casi todos los proyectos open source albergados en GitHub o GitLab.

Pasos a realizar para colaborar:

1. Crear usuario en GitLab.com
2. Acceder al proyecto (p.e: https://gitlab.com/LoB/LoB.Home) y realizar un `fork` a tu cuenta. 
`Fork` es el proceso en el cual gitlab hace una copia de un proyecto a la cuenta del usuario que lo realiza. Al terminar el proceso te crea un proyecto en la url https://gitlab.com/[nombre-usuario]/[nombre-proyecto], si el usuario se llama `Barnabas` al clonar el proyecto https://gitlab.com/LoB/LoB.Home se creará el proyecto https://gitlab.com/Barnabas/LoB.Home
3. Una vez tenemos la copia del proyecto en la cuenta de nuestro usuario podemos realizar los cambios pertinentes.
4. Cuando los cambios estén finalizados y se desé pasar dichos cambios al repositorio original habrá que realizar un `Merge Request`

## Gía de colaboración en la wiki

La wiki está situada en la carpeta [wiki](wiki). La wiki se escribe usando [Markdown](https://es.wikipedia.org/wiki/Markdown) para no requerir de ninguna aplicación específica para poder colaborar y poder ver los resultados facilmente en la web. Los archivos Markdown tienen extensión `.md`.  

La wiki tiene una estructura arborescente, que se refleja también en las carpetas del proyecto por ejemplo, la estructura arborescente puede crecer y ser modificada reflejando los cambios en los archivos y carpetas del proyecto. En la estructura podrán haber secciones planificadas que no estén todavía escritas, dichas secciones no tendrán enlace a ningun archivo.

A continuación se muestra un ejemplo de como se reparten los contenidos en archivos:

1. Alcance
2. Parte técnia -> (wiki/parte-tecnica.md)
  1. Arquitectura -> (wiki/parte-tecnica/arquitectura.md)
  2. Guía de desarrollo
      1. Estructura de una solución Visual Studio para una aplicación -> (wiki/parte-tecnica/guia-de-desarrollo/estructura-de-una-solucion.md)
      2. Proyectos -> (wiki/parte-tecnica/guia-de-desarrollo/proyectos.md)
      3. Nomenclatura de clases.
3. Parte funcional
  1. Áreas de negocio
  2. Procesos

Los puntos 1, 1.2, 1.3 y 3.* no tienen todavía un documento escrito, están planificados, el resto de puntos si tienen un enlace a documento, quiere decir que tienen contenido.

Debido a las restricciones del sistema de archivos y de la composición de urls para tener acceso directo a los archivos, hay que componer los los nombres de las carpetas y de los archivos de la siguiente forma:

1. Se cambian los espacios en blanco por guiones simples `-`
2. Se usan letras en minusculas.
3. Se cambian las vocales acentuadas por las correspondientes sin acentuar
4. Los archivos tienen extensión `.md`

