# Proyectos

Los proyectos son un conjunto de ficheros de código con un cometido común. Estos pueden corresponderse o bien con un proyecto de Visual Studio o bien un repositorio Git que podría ser incluido en el repositorio final de la aplicación como un [submódulo](https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Subm%C3%B3dulos).

# Nombres de proyecto

Todos los proyectos del núcleo empiezan por `LoB.` seguidos por el nombre elegido para el cometido del proyecto, si dentro de un proyecto se llega a la necesidad de dividir ese proyecto en varios se pueden ir concatenando los nombres elegidos separados por un punto.  

Ejemplos proyectos base:
* LoB.Core (Código del núcleo)
* LoB.Warehouse (Código con entidades y reglas de negocio de procesos de almacén)
* LoB.EF (Código especifico de Entity Framework)
* LoB.Web (Código referente al uso de System.Web)

Ejemplos especializaciones:
* LoB.Core.Resources (Recursos del núcleo)
* LoB.Core.ABCPdf (Código del núcleo con funciones genéricas para trabajar con [ABCPdf](http://www.websupergoo.com/abcpdf-1.htm))
* LoB.Warehouse.EF (Código especifico de entity framework de para el almacén)
* LoB.Web.Client (Código para el browser web (javascript, css, ...))

Los proyectos se van creando en función de las necesidades, pero el modo más claro de ver que se necesita crear un proyecto aparte suele ser la necesidad en añadir una referencia a ensamblados que no forman parte de .Net Framework, o que si forman parte de este tienen una entidad mayor que trabarían la escalabilidad de las aplicaciones LoB, al estilo de `System.Web`. 

Para esto hay que tener en cuenta cual es la responsabilidad de cada proyecto y extraer de él el código que responda a otras responsabilidades. Así pues se pueden ir creando proyectos dependientes de los anteriores para tareas especializadas dentro del proyecto anterior sin necesidad de incluir referencias externas.

Ejemplos de especializaciones sin referencia externa:
* LoB.Core.Localization (tratamiento de formatos de fecha, números, ...)
* LoB.Core.Internazionalization
* LoB.Core.Generators

En muchas ocasiones no existirá la necesidad de crear un nuevo proyecto debido a la falta de entidad de la tarea en cuestión, de tal modo se ubicará el código en una carpeta con un nombre descriptivo.

# Estructura del proyecto .NET Librería de clases

Los proyectos LoB siempre tienen como espacio de nombres por defecto `LoB` de forma que según vayamos incluyendo carpetas las clases tendrán en el namespace el nombre de la carpeta y su nombre completo será `LoB.[Carpetas].[NombreClase]`, por ejemplo la clase `User` situada en la carpeta `Domain\Entities` será `LoB.Domain.Entities.User`.

Las carpetas se organizarán por [capas](wiki/parte-tecnica/arquitectura.md), por lo tanto las clases que pertenezcan a una capa concreta deberán encontrarse bajo la carpeta del nombre de la capa, las carpetas de las capas son:

1. Application (Capa de aplicación)
2. Domain (Capa de dominio)
3. Infrastructure (Capa de infraestructura)

Las carpetas nunca deben de estar vacías, si está vacía se elimina la carpeta para quitar ruido.

Los servicios que se escriban dentro de cada una de esas capas deben meterse en la subcarpeta `Services`.
Cuando no se tiene claro si una clase o conjunto de ellas, pertenecen a una de estas capas, se prefiere crear una nueva carpeta en la raíz que describa el cometido del código que incluye. Por ejemplo:

1. Configuration. (Clases de configuración de la aplicación)
2. DependencyResolution. (Clases para la IoC y DI)
3. Util (Clases con utilidades varias)
3. Util/Extensions (Clases con método extensores)
4. Util/Lambda (Clases para el manejo de Lambdas)

## Carpeta Domain

Dentro de la carpeta domain irán las definiciones de la entidades de negocio y los servicios, estas se estructurarán en la carpeta `Entities` y `Services` respectivamente. La definición de las consultas a la base de datos se ubicarán en la carpeta `QueryInfos` y los eventos del dominio irán a la carpeta `Events`, de tal modo que quedaría así:

* Domain
  * Entities (Entidades)
  * Events (Eventos)
  * QueryInfos (Consultas predefinidas)
  * Services (Servicios)

Se podrán añadir más carpetas según las necesidades.

## Carpeta Application

Dentro de la carpeta de aplicación estarán los servicios bajo la subcarpeta `Services`. La capa de aplicación tiene la peculiaridad de trabajar con `DTOs`, así que estos irán dentro de la carpeta `Dtos`, quedaría así:

* Application
  * Dtos (Clases con definiciones de DTOs)
  * Services (Servicios de aplicación)

Se podrán añadir más carpetas según las necesidades.

## Carpeta Infrastructure

Esta carpeta es la menos rígida debido a su naturaleza, se podrán crear subcarpetas dependiendo de la especialización.  
Aún así de esta carpeta se define la carpeta `Services` donde deben de ir los servicios de infraestructura. Cuando existan varias clases que trabajen con el mismo `agente externo` se podrán agrupar en subcarpetas.

Podremos así organizar el código de la siguiente forma:

* Infrastructure
  * Services
      * EntitySerializationService.cs
      * Windows
          * FileService.cs
          * DirectoryService.cs
      * Unix
          * FileService.cs
          * DirectoryService.cs

O también:

* Infrastructure
  * Serialization
      * EntitySerializationService.cs
  * Windows
      * FileService.cs
      * DirectoryService.cs
  * Unix
      * FileService.cs
      * DirectoryService.cs

O también si se desea:

* Infrastructure
  * Services
      * EntitySerializationService.cs
  * Windows
      * FileService.cs
      * DirectoryService.cs
  * Unix
      * FileService.cs
      * DirectoryService.cs

## Ubicación de clases base conceptualmente abstractas

Las clases base abstractas, en cuanto a concepto y no a código, debe de situarse en una subcarpeta `Base`, de modo que:

* Las clases base de las entidades estarán en `Domain/Entities/Base`
* Las clases base de los servicios en `[Domain|Application|Infrastructure]/Services/Base`

Y así sucesivamente.
Deben ser clases abstractas en cuanto a concepto, conceptos abstractos al dominio. Por ejemplo la clase base de las entidades `Entity` es una clase conceptualmente abstracta, de las que deriban `User`, `Role`, `Invoice`, ..., sin embargo la clase base de movimiento de almacén `StockLog` no es abstracta por que en el dominio existe un elemento llamado `StockLog` que representa un movimiento de almacén, aunque luego existan las clases derivadas `StockLogInput` y `StockLogOutput` que son especializaciones de la anterior y también elementos del dominio.

# Estructura del proyecto ejecutable

Estos suelen ser proyectos de la capa de presentación, debido a la naturaleza de dichos proyectos y a los que Visual Studio suele imponer unas convenciones o carpetas estándar debemos adaptarnos a ellos lo mejor posible.
 
Dentro de lo posible y de la tipología del proyecto ejecutable, se seguirán las reglas de estructura del [proyecto .Net](#estructura-del-proyecto-net-librer%C3%ADa-de-clases).
