# Nomenclatura de clases

Los nombres de las clases deben describir el cometido de dicha clase, dado que existen distintos tipos de clases lo suyo sería componer el nombre con el nombre funcional y el tipo de clase que es, te tal modo que:

* `UserEntity` - Sería la clase de la entidad `User`
* `UserDomainService` - Sería la clase del servicio de negocio de usuarios.
* `UserApplicationService` - Sería la clase del servicio de apliación de usuarios.
* `UserFilesInfrastructureService` - Sería el nombre del servicio de infraestructura que accede a los ficheros de un usuario.

Estos nombres de clases serían los ideales, pero dado que tenemos una [escructura de proyecto](wiki/parte-tecnica/guia-de-desarrollo/proyectos.md) que nos va diciendo que es cada cosa, la cual es equiparable a los namespaces, reduciremos los nombres de las clases de la siguiente forma.

## Nombres de clases de entidad

Las clases de las entidades según la [escructura de proyecto](wiki/parte-tecnica/guia-de-desarrollo/proyectos.md#carpeta-domain) deben caer en el carpeta Domain/Entities, por lo tanto una clase de entidad de usuario, `UserEntity`, cuyo namespace es `Domain.Entities` daría como resultado `Domain.Entities.UserEntity` ... donde se puede ver que `Entity` es redundante.

Por lo tanto las clases de entidades del dominio son nombradas con el nombre de la entidad sin más. La clase de entidad de usuario se llamará `User`, la clase de entidad para albaranes se llamará `DeliveryNote`, la clase de factura se llamará `Invoice`. 

La única excepción sería cuando la clase de entidad no esté situada en la carpeta `Domain/Entities`, cosa que debería estar también justificada, pero si fuera el caso entonces si sería correcto usar el sufijo `Entity`.

## Nombres de clases de servicio de dominio y aplicación.

Los nombres de las clases de servicio se ven afectadas por la misma norma que las clases de entidad, crear una clase llamada `Domain.Services.UserDomainService` es redundante, así que optamos por llamarles por su nombre funcional con el sufijo `Service`, el hecho de diferenciar que tipo de servicio es lo podemos obtener mirando el namespace. En el caso de colisión de nombres entonces si deberemos especificar en el nombre el tipo de servicio que es. 

Por ejemplo, si tenemos una clase de entidad llamada `User`, su clase de dominio se llamará `UserService`, en el caso de que tuvieramos que crear una clase de servicio de aplicación exclusiva para usuarios esta se debería llamar `UserApplicationService`.

```
[__Presentation_________Layer______]
  \  \ /  /        |    \  \ /  /
 AppService1       |   AppService2
   /     \  \____  |    /   \
   |      |      \  \  /     \
DomSrv1 DomSrv2  DomSrv3   DomSrv4

``` 










`Infrastructure.Services.Windows.FileWindowsInfraestructureService` es redundante, 